<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        //Categories seeding
        for($i=0; $i < 3; $i++)
        {
            /*
            DB::table('categories')->insert([
                'category' => Str::random(10),
                'users_id' => rand(1,3)
            ]);
               
              
            DB::table('subcategories')->insert([
                'subcategory' => Str::random(10),
                'categories_id' => rand(1,3),
                'users_id' => rand(1,3)
            ]);

            DB::table('accounts')->insert([
                'account' => Str::random(10),
                'fuc' => Str::random(10),
                'key' => Str::random(10),
                'users_id' => rand(1,3)
            ]);*/ 
            
/*
            DB::table('courses')->insert([
                'course' => Str::random(10),
                'users_id' => rand(1,3)
            ]);*/
        
            
            DB::table('payments')->insert([
                'order' => Str::random(10),
                'title' => Str::random(10),
                'description' => Str::random(50),
                'price' => rand(10,100)/10,
                'started_at' => date('Y-m-d'),
                'ended_at' => date('Y-m-d'),
                'accounts_id' => rand(1,3),
                'subcategories_id' => rand(1,3),
                'courses_id' => rand(1,3),
                'users_id' => rand(1,3),
                'categories_id' => rand(1,3)
            ]);
            
        }

        //Payments seeding

        //Course seeding

        //Account
    }
}
