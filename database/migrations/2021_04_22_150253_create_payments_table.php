<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->string('order', 50);
            $table->string('title', 150);
            $table->mediumText('description');
            $table->decimal('price', 6, 2 );
            $table->date('started_at');
            $table->date('ended_at');
            $table->bigInteger('categories_id')->unsigned()->nullable();
            $table->foreign('categories_id')->references('id')->on('categories');
            $table->bigInteger('accounts_id')->unsigned()->nullable();
            $table->foreign('accounts_id')->references('id')->on('accounts');
            $table->bigInteger('subcategories_id')->unsigned()->nullable();
            $table->foreign('subcategories_id')->references('id')->on('subcategories');
            $table->bigInteger('courses_id')->unsigned()->nullable();
            $table->foreign('courses_id')->references('id')->on('courses');
            $table->bigInteger('users_id')->unsigned()->nullable();
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
