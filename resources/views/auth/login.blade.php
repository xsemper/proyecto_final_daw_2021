@extends('layouts.appauth')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Outer Row -->
<div class="row justify-content-center">

    <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-6 d-none d-lg-block loginimg">
                    </div>
                    <div class="col-lg-6">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Benvingut al portal Ins Camí de Mar!</h1>
                            </div>
                            <form class="user" method="POST"  id="login_form" action="{{ route('login') }}">
                                @csrf
                               <div class="form-group">
                                    <label for="email"
                                        class="col-md-6 col-form-label">{{ __('Correu electrónic') }}</label>
                                    <input id="email" type="email"
                                        class="form-control form-control-user @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password" class="col-md-6 col-form-label">{{ __('Contrassenya') }}</label>
                                    <input id="password" type="password"
                                        class="form-control form-control-user @error('password') is-invalid @enderror"
                                        name="password" required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember"
                                                id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __("Recorda'm") }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <script>
                                    function getaction() {
                                        $("#login_button").prop('disabled', false);
                                    }
                                    document.getElementById("login_form").addEventListener("submit",function(evt) {
                                        var response = grecaptcha.getResponse();
                                        if(response.length == 0) { 
                                            //reCaptcha not verified
                                            alert("Porfavor, verifica que eres una persna humana!");
                                            evt.preventDefault();
                                            return false;
                                        } else {
                                            
                                        }
                                        
                                    });
                                    

                                </script>
                                <div class="g-recaptcha" data-callback="getaction" data-sitekey="6Lct7dEaAAAAAMfVvu-CcdS1ZYLu0Pt0dxaLG8kU"></div>
                                <span id="captcha" style="margin-left:100px;color:red" />
                                
                                <br>
                                <div class="form-group row mb-0">
                                    <div class="col-md-12">
                                        <button id="login_button" class="btn btn-primary btn-user btn-block" disabled>
                                            {{ __('Login') }}
                                        </button>
                                        <hr>

                                        <a href="{{ url('auth/google') }}" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login amb Google
                                        </a>
                                        
                                        <hr>

                                        <a href="{{ url('home') }}" class="btn btn-primary btn-user btn-block">
                                            Portal
                                        </a>

                                        <hr>

                                        @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Has perdut la contrassenya?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
