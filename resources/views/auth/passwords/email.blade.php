@extends('layouts.appauth')
@section('content')

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg-6 d-none d-lg-block loginimg"></div>
                        <div class="col-lg-6">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-2">¿Has olvidado tu contraseña?</h1>
                                    <p class="mb-4">No te preocupes. Te enviaremos un e-mail de confirmación a tu cuenta de correo y podrás cambiar tu contraseña</p>
                                </div>
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <form class="user" method="POST" action="{{ route('password.email') }}">
                                    @csrf
                                    <div class="form-group  offset-md-1">
                                        <label for="email"
                                            class="col-md-8 col-form-label">{{ __('Dirección de correo') }}</label>

                                        <div class="col-md-11">
                                            <input id="email" type="email"
                                                class="form-control @error('email') is-invalid @enderror form-control-user "
                                                name="email" value="{{ old('email') }}" required
                                                autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group offset-md-1">
                                        <div class="col-md-11">
                                            <button type="submit" class="btn btn-primary btn-user btn-block textoBoton">
                                                {{ __('Resetear contraseña') }}
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group offset-md-1">
                                        <div class="col-md-11">
                                            @if (Route::has('login'))
                                                <a class="btn btn-link" href="{{ route('login') }}">
                                                    {{ __('Ya he recordado la contraseña') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
