@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifica el teu E-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nou link de confirmació ha sigut enviat al teu correu.') }}
                        </div>
                    @endif

                    {{ __('Abans de continuar verifica que has rebut un e-mail de confirmació.') }}
                    {{ __('Si no has rebut e-mail') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('Prem aquí per demanar un altre') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
