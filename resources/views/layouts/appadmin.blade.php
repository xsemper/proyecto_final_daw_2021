<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Index</title>

    <!-- Base plugins and styles -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <link href="{{ asset('css/own.css' )}}" rel="stylesheet">

    <!-- notification files -->
    <link href="{{ asset('css/notifications.css' )}}" rel="stylesheet">
    <script src="{{ asset('js/notifications.js')}}"></script>


    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css' )}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js')}}"></script>
    <script language="javascript" type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.10.0/localization/messages_es.js"></script>

    <!-- Botones DataTalbe-->
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.0/js/buttons.html5.min.js"></script>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Logo -->
            <li>
            <img src="{{ asset('img/logo.png')}}" alt="LogotipoInstituto" style="max-width: 100%">
            </li>

            <!-- Divider -->
            <li>
                <hr class="sidebar-divider my-0">
            </li>

            <!-- Sidebar - Brand -->
            <li>
                <a class="sidebar-brand d-flex align-items-center justify-content-center">
                    <div class="sidebar-brand-text mx-3">Admin</div>
                </a>
            </li>

            <!-- Divider -->
            <li>
                <hr class="sidebar-divider my-0">
            </li>

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="{{ url('home') }}">
                    <b><span class="text-white">Pantalla Principal</span></b>
                </a>
            </li>

            <!-- Divider -->
            <li>
                <hr class="sidebar-divider">
            </li>
            <!-- Heading -->
            <li>
                <div class="sidebar-heading text-white">
                    Opcions
                </div>
            </li>
            <!-- Nav -->
            <!-- Categorias -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/admincategories/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Categories</span></b>
                </a>
            </li>

            <!-- Pagaments -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/adminpayments/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Pagaments</span></b>
                </a>
            </li>

            <!-- Subcategories -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/adminsubcategories/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Subcategories</span></b>
                </a>
            </li>

            <!-- Accounts -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/adminaccounts/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Comptes</span></b>
                </a>
            </li>

            <!-- Courses -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/admincourses/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Cursos</span></b>
                </a>
            </li>
            @if(Auth::user()->role === 2)
            <!-- Users -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="{{ url('/adminusers/index')}}">
                    <i class="fas fa-fw fa-folder"></i>
                    <b><span class="textoAzulIns">Usuaris</span></b>
                </a>
            </li>
            @endif

            <!-- Divider -->
            <li>
                <hr class="sidebar-divider d-none d-md-block">
            </li>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-dark bg-dark topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw"></i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>
                        <li>
                            <div class="topbar-divider d-none d-sm-block"></div>
                        </li>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-white">{{Auth::user()->name}}</span>
                                <img class="img-profile rounded-circle" alt="ProfilePic" src="/img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" data-toggle="modal" data-target="#logoutModal" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400">
                                    </i>Logout
                                    <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form> 
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->


                <!-- Comienzo del contenido de la pagina -->
                <div class="container-fluid">
                    @yield('content')
                </div>
                <!-- Finalización del contenido de la pagina -->

                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span><a href="https://www.inscamidemar.cat/">Ins cami de mar</a>. Calafell</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>
        <!-- End of Page Wrapper -->

        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
<script>
    function borrarConfirm() {
        var opcion = confirm("¿Estas seguro de que deseas borrar ese elemento?");
        if (opcion == true) {
            return true;
        } else  {
            return false;
        }
    } 
</script>