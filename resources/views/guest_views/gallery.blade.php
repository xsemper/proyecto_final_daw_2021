@extends('layouts.appguest')
@section('content')
<!-- Comienzo carta explicación -->
@foreach($courses as $course)
<h2>{{$course->course}}</h2>
<div class="row">
    <!-- Foreach -->

    @foreach($payments as $payment)
    @if($payment->courses_id === $course->id)
    <div class="col-xl-4 col-lg-6 col-xs-12 col-md-12">
        <div class="card shadow mb-4">
            <!-- Card Header - Titulo -->
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">{{$payment->title}}</h6>
            </div>
            <!-- Inicio descripción -->
            <div class="col-12">
                <br><br>
                <div class="mb-4">
                    <div>{!! nl2br(e($payment->description)) !!}</div>
                </div>
            </div>
            <!-- Final descrpición-->
            <!-- Carta para el precio -->
            <div class="card border-left-success shadow h-100 py-2 col-xl-8 col-8 col-md-8 col-sm-4 ml-3 mr-3">
                <div class="card-body ">
                    <div class="row no-gutters align-items-center">
                        <div class="col-xs-10 mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Precio</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{$payment->price}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-euro-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin carta del precio -->
            <!-- Card Body -->
            <div class="card-body">
                <hr>
                <!-- Botón para ir a pagos -->
                <div class="row">
                    <div class="col-5">
                    <a class="text" href="{{ url('/paymentsgallery/show/'.$payment->id)}}">
                        <span class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                                <i class="fas fa-info-circle"></i>
                            </span>
                            <span class="text">Més detalls</span>
                        </span>
                    </a>
                    </div>
                    <!-- Sermepa -->
                    <div class="col-5 mr-1">
                        @if($payment->ended_at >= now()->toDateString() && $payment->started_at <= now()->toDateString())
                        <span class="btn btn-warning btn-icon-split" href="javascript: void(0)"
                            onclick="document.getElementById('form_1').submit();">
                            <span class="icon text-white-50">
                                <i class="fas fa-credit-card"></i>
                            </span>
                            <span class="text" >Pagament </span>
                            <form action="https://sis.sermepa.es/sis/realizarPago" method="post"
                                accept-charset="utf-8" id="form_1"></form>
                            <input type="hidden" name="Ds_SignatureVersion" value="HMAC_SHA256_V1" />
                            <input type="hidden" name="Ds_MerchantParameters"
                                value="eyJEU19NRVJDSEFOVF9BTU9VTlQiOiIyMDAwMCIsIkRTX01FUkNIQU5UX09SREVSIjoiMjAwMjI3MTAyOTU0IiwiRFNfTUVSQ0hBTlRfTUVSQ0hBTlRDT0RFIjoiMDIyMzE2Nzk5MCIsIkRTX01FUkNIQU5UX0NVUlJFTkNZIjoiOTc4IiwiRFNfTUVSQ0hBTlRfVFJBTlNBQ1RJT05UWVBFIjoiMCIsIkRTX01FUkNIQU5UX1RFUk1JTkFMIjoiMSIs" />
                        @else
                            <span class="btn btn-secondary btn-icon-split">
                            
                            <span class="icon text-white-50">
                                <i class="fas fa-credit-card"></i>
                            </span>
                            <span class="text">Expirado</span>
                            
                            </span>
                        @endif
                    </div>
                </div>
                <!-- Fin Sermpea -->
            </div>
        </div>
    </div>
    @endif
    @endforeach
</div>
@endforeach
<!-- Finalización del contenido de la pagina -->
<script>
    $(document).ready(function () {

        $('#input-summernote').summernote({ // Para el wysiwyg
            placeholder: 'placeholder test',
        });

        $('#save-summernote').click(function () {
            var id = $(this).attr('data-id');
            var $summernote = $("#input-summernote");
            var text = $summernote.summernote("code");

            $.ajax({
                url: '/costs/updatesm',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    'desc': text,
                    'id': id
                },
                dataType: "json",
                success: function (data) {
                    $summernote.summernote("code", data);
                }
            });

        });

    });

</script>
@endsection
