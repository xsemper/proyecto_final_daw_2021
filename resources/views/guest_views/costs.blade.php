@extends('layouts.appguest')
@section('content')
<h1 class="h3 mb-4 text-gray-800">{{$payment->title}}</h1>
<!-- Comienzo carta explicación -->
<div class="row justify-content-center align-items-center">
    <div class="col-lg-6">
        <!-- Basic Card Example -->
        <div class="card shadow mb-12">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Benvolgudes Famílies</h6>
            </div>
            <div class="card-body">{!! nl2br(e($payment->description)) !!}</div>
            <!-- Fin summernote -->
            <!-- Fin carta explicación -->
            <!-- Carta de pago -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-success shadow h-100 py-2">
                    <div class="card-body ">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                    Precio</div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800">{{$payment->price}}</div>
                            </div>
                            <div class="col-auto">
                                <i class="fas fa-euro-sign fa-2x text-gray-300"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Fin carta de pago -->
            <!-- Sermepa -->
            @if($payment->ended_at >= now()->toDateString() && $payment->started_at <= now()->toDateString())
            <span class="btn btn-warning btn-icon-split" href="javascript: void(0)" onclick="document.getElementById('form_1').submit();">
                <span class="icon text-white-50">
                    <i class="fas fa-credit-card"></i>
                </span>
                <span class="text" >Pagament</span>
                <form action="https://sis.sermepa.es/sis/realizarPago" method="post"
                    accept-charset="utf-8" id="form_1"></form>
                <input type="hidden" name="Ds_SignatureVersion" value="HMAC_SHA256_V1" />
                <input type="hidden" name="Ds_MerchantParameters"
                    value="eyJEU19NRVJDSEFOVF9BTU9VTlQiOiIyMDAwMCIsIkRTX01FUkNIQU5UX09SREVSIjoiMjAwMjI3MTAyOTU0IiwiRFNfTUVSQ0hBTlRfTUVSQ0hBTlRDT0RFIjoiMDIyMzE2Nzk5MCIsIkRTX01FUkNIQU5UX0NVUlJFTkNZIjoiOTc4IiwiRFNfTUVSQ0hBTlRfVFJBTlNBQ1RJT05UWVBFIjoiMCIsIkRTX01FUkNIQU5UX1RFUk1JTkFMIjoiMSIs" />
            @else
                <span class="btn btn-secondary btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-credit-card"></i>
                </span>
                <span class="text">Expirado</span>
                </span>
            @endif
            <!-- Fin Sermpea -->
        </div>
    </div>

</div><br>
<div class="row justify-content-center align-items-center">
    <!-- Basic Card Example -->
    <br>
    <div class="row m-2">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 col-3">
            <a href="http://twitter.com/intent/tweet?url=Mira este pago activo {{url()->current()}}"
                class="btn btn-info btn-icon-split">
                <span class="icon text-white-50" style="padding: .575rem .75rem;">
                    <i class="fab fa-twitter"></i>
                </span>
                <span class="text">Twitter</span>
            </a>
        </div>
    </div>
    <div class="row m-2">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 col-3">
            <a href="http://facebook.com/sharer.php?u={{url()->current()}}"
                class="btn btn-primary btn-icon-split">
                <span class="icon text-white-50" style="padding: .575rem .75rem;">
                    <i class="fab fa-facebook"></i>
                </span>
                <span class="text">Facebook</span>
            </a>
        </div>
    </div>
    <div class="row m-2">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 col-3">
            <a href="https://api.whatsapp.com/send?text=Mira los pagos activos de {{url()->current()}}"
                class="btn btn-success btn-icon-split">
                <span class="icon text-white-50" style="padding: .575rem .75rem;">
                    <i class="fab fa-whatsapp"></i>
                </span>
                <span class="text">WhatsApp</span>
            </a>
        </div>
    </div>
    <div class="row m-2">
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-3 col-3">
            <a href="tg:msg_url?url={{url()->current()}};text=Pagaments de l'Ins Camí de Mar"
                class="btn btn-info btn-icon-split">
                <span class="icon text-white-50" style="padding: .575rem .75rem;">
                    <i class="fab fa-telegram"></i>
                </span>
                <span class="text">Telegram</span>
            </a>
        </div>
    </div>
    <!-- Finalización del contenido de la pagina -->
    @endsection
