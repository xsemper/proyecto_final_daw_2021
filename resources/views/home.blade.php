
@extends('layouts.appguest')
@section('content')
    <h1 class="h3 mb-4 text-gray-800">HOME</h1>
    <!-- Comienzo carta explicación -->
    <div class="row justify-content-center align-items-center">
        <div class="col-lg-6">
            <!-- Basic Card Example -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">HOME</h6>
                </div>
                <div class="card-body">
                    Aquesta és la pàgina principal dels pagaments online de l'INS Camí de Mar de Calafell, per a procedir a realitzar un pagament, escull el curs al menú de la barra blava superior i fes clic al nom del pagament que vulguis realitzar per a continuar amb el tràmit
                </div>
            </div>
            <br><br>
        </div>
    </div>
<!-- Finalización del contenido de la pagina -->
@endsection

            