@extends('layouts.appadmin')

@section('content')
<!-- Comienzo del contenido de la pagina -->
<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Usuaris</h1>
    <!-- Comienzo tabla -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Taula de Usuaris</h6>
            <a href="{{url('register')}}"><button class="btn btn-primary">Registrar Usuari</button></a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table_id" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Usuari</th>
                            <th>E-mail</th>
                            <th>Estat</th>
                            <th>Rol</th>
                            <th>Google ID</th>
                            <th>Accions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->status == 1 ? 'Actiu' : 'Inactiu'}}</td>
                            <td>{{$user->role}}</td>
                            <td>{{$user->google_id}}</td>
                            <td>
                                @if(Auth::user()->role == 2)
                                <a class="edit" data-id="{{$user->id}}" data-toggle="modal"
                                    data-target="#exampleModal"><i class="fas fa-edit"></i></a>
                                <a class="delete" data-id="{{$user->id}}"><i class="fas fa-trash-alt"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Finalización tabla -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Editar un usuari</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <form id="formAccounts" name="formAccounts" method="post" action="{{url('/adminusers/update')}}"
                        style="display: block;">
                        @csrf
                        <input type="hidden" name="input_id" id="input_id">
                        <label for="input_role">Rol:</label>
                        <SELECT name="input_role" id="input_role">
                            <option value="0">Invitat</option>
                            <option value="1">Administrador</option>
                            <option value="2">Superadministrador</option>
                        </SELECT><br><br>
                        <label for="input_status">Estat:</label>
                        <SELECT name="input_status" id="input_status">
                            <option value="0">Inactiu</option>
                            <option value="1">Actiu</option>
                        </SELECT><br><br>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="modal-close" name="modal-send"
                                data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="modal-send"
                                name="modal-send">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Finalización del contenido de la pagina -->

<script>
    $(document).ready(function () {

        //VALIDATION
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "Si us plau, no fiquis separacions");

        jQuery.validator.addMethod("customEmail", function (value, element) {
            return this.optional(element) ||
                /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
        }, "Please enter valid email address!");

        $.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || /^\w+$/i.test(value);
        }, "Letras, numeros y barra-bajas porfavor.");

        var $formAccounts = $('#formAccounts');

        if ($formAccounts.length) {
            $formAccounts.validate({
                rules: {
                    categoria: {
                        required: true
                    },
                    nivell: {
                        required: true
                    },
                    titol: {
                        required: true
                    },
                    preu: {
                        required: true,
                        digits: true
                    },
                    dataInicial: {
                        required: true,
                        noSpace: true
                    },
                    dataFinal: {
                        required: true,
                        noSpace: true
                    }
                },
                messages: {
                    categoria: {
                        required: 'Si us plau, introdueix una categoria!'
                    },
                    nivell: {
                        required: 'Si us plau, introdueix un nivell!'
                    },
                    titol: {
                        required: 'Si us plau, introdueix un titol!'
                    },
                    preu: {
                        required: 'Si us plau, introdueix un preu!',
                        digit: 'Si us plau, introdueix solamente números.'
                    },
                    dataInicial: {
                        required: 'Si us plau, introdueix una data!'
                    },
                    dataFinal: {
                        required: 'Si us plau, introdueix una data!'
                    }
                },
            });
        }

        //DATA TABLE
        $('#table_id').DataTable({
            dom: 'Bflrtrip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
                }
            ],
            "language": {
                "sProcessing": "Processant...",
                "sLengthMenu": "Mostrar _MENU_ registres",
                "sZeroRecords": "No s'han trobat resultats",
                "sEmptyTable": "Cap dada disponible en aquesta taula",
                "sInfo": "Mostrant registres del _START_ al _END_ d'un total de _TOTAL_ registres",
                "sInfoEmpty": "Mostrant registres del 0 al 0 d'un total de 0 registres",
                "sInfoFiltered": "(Filtrat d'un total de _MAX_ registres)",
                "sInfoPostFix": "",
                "sSearch": "Cercar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargant...",
                "oPaginate": {
                    "sFirst": "Primer",
                    "sLast": "Últim",
                    "sNext": "Següent",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar per ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per ordenar la columna de manera descendent"
                }
            }
        });

        //DATA TABLE FUNCTIONALITIES
        $('#table_id').on('click', '.delete', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            const successNotf = window.createNotification({
                theme: 'success',
                showDuration: 3000
            });
            const errorNotf = window.createNotification({
                theme: 'error',
                showDuration: 3000
            });
            
            if (borrarConfirm()) {
               /* myTable.row($(this).parents('tr'))
                    .remove()
                    .draw(); */

                $.ajax({
                    url: '/adminusers/delete',
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        id: id,
                    },
                    dataType: "json",
                    success: function (data) {
                        setTimeout(location.reload(), 500);
                        successNotf({
                            message: 'Eliminat correctament.'
                        });
                        
                    },
                    error: function () {
                        errorNotf({
                            message: "No s'ha pogut eliminar."
                        });
                    }
                });
            }
        });

        //DATA TABLE EDIT
        $('#table_id').on('click', '.edit', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            $.ajax({
                url: '/adminusers/find',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    id: id,
                },
                dataType: "json",
                success: function (data) {
                    $("#input_id").val(data.id);
                    $("#input_role").val(data.role);
                    $("#input_status").val(data.status);
                }
            });
        });
    });
</script>
@endsection
