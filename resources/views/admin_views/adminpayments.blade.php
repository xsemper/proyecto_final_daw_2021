@extends('layouts.appadmin')
@section('content')
<h1 class="h3 mb-4 text-gray-800">Pagaments</h1>
<!-- Comienzo tabla -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Taula de Pagaments</h6>
        <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Afegir Pagament</button>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table_id" class="table table-bordered display">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Títol</th>
                        <th>Comanda</th>
                        <th>Preu</th>
                        <th>Comença el</th>
                        <th>Acaba el</th>
                        <th>Compte</th>
                        <th>Categoria</th>
                        <th>Subcategoria</th>
                        <th>Curs</th>
                        <th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payment as $payment)
                    <tr>
                        <td>{{$payment->id}}</td>
                        <td>{{$payment->title}}</td>
                        <td>{{$payment->order}}</td>
                        <td>{{$payment->price}}</td>
                        <td>{{$payment->started_at}}</td>
                        <td>{{$payment->ended_at}}</td>
                        <td>{{isset($payment->account()->account) ? $payment->account()->account : '' }}</td>
                        <td>{{isset($payment->category()->category) ? $payment->category()->category : '' }}</td>
                        <td>{{isset($payment->subcategory()->subcategory) ? $payment->subcategory()->subcategory : '' }}
                        </td>
                        <td>{{isset($payment->course()->course) ? $payment->course()->course : '' }}</td>
                        <td>
                            <a class="edit" data-id="{{$payment->id}}" data-toggle="modal"
                                data-target="#exampleModal"><i class="fas fa-edit"></i></a>
                            <a class="delete" data-id="{{$payment->id}}"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Finalización tabla -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Afegir un pagament</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <form class="form-horizontal" id="formPagaments" name="formPagaments" method="POST"
                    action="/adminpayments/insertorupdate" style="display: block;">
                    @csrf
                    <input type="hidden" name="input_id" id="input_id">
                    <div class="form-group mb-0">
                        <label for="input_title">Títol:</label>
                        <input class="form-control form-control-user" type="text" name="input_title" id="input_title" />
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_order">Comanda:</label>
                        <input class="form-control" type="text" name="input_order" id="input_order" />
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_description">Descripció:</label>
                        <textarea class="form-control" type="text" name="input_description" id="input_description"></textarea>
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_category">Categoria:</label>
                        <SELECT class="form-control" name="input_category" id="input_category">
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->category}}</option>
                            @endforeach
                        </SELECT>
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_subcategory">Subcategoria:</label>
                        <SELECT class="form-control" name="input_subcategory" id="input_subcategory" disabled>
                            <!-- Jquery data -->
                        </SELECT>
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_price">Preu:</label>
                        <input class="form-control" type="text" name="input_price" id="input_price" />
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_startedat">Comença el:</label>
                        <input class="form-control" type="date" name="input_startedat" id="input_startedat" />
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_endedat">Acaba el:</label>
                        <input class="form-control" type="date" name="input_endedat" id="input_endedat" />
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_account">Compte:</label>
                        <SELECT class="form-control" name="input_account" id="input_account">
                            @foreach($accounts as $account)
                            <option value="{{$account->id}}">{{$account->account}}</option>
                            @endforeach
                        </SELECT>
                    </div>

                    <div class="form-group mb-0">
                        <label for="input_course">Curs:</label>
                        <SELECT class="form-control" name="input_course" id="input_course">
                            @foreach($courses as $course)
                            <option value="{{$course->id}}">{{$course->course}}</option>
                            @endforeach
                        </SELECT>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                        <button type="submit" class="btn btn-primary">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Finalización del contenido de la pagina -->

<script>
    $(document).ready(function () {
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "Si us plau, no fiquis separacions");

        jQuery.validator.addMethod("customEmail", function (value, element) {
            return this.optional(element) ||
                /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
        }, "Please enter valid email address!");

        $.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || /^\w+$/i.test(value);
        }, "Letras, numeros y barra-bajas porfavor.");

        var $formPagaments = $('#formPagaments');

        if ($formPagaments.length) {
            $formPagaments.validate({
                rules: {
                    input_title: {
                        required: true
                    },
                    input_order: {
                        required: true
                    },
                    input_startedat: {
                        required: true
                    },
                    input_price: {
                        required: true,
                        number: true
                    },
                    input_endedat: {
                        required: true,
                        noSpace: true
                    },
                    input_course: {
                        required: true,
                        noSpace: true
                    }
                },
                messages: {
                    input_title: {
                        required: 'Si us plau, introdueix un títol!'
                    },
                    input_order: {
                        required: 'Si us plau, introdueix una comanda!'
                    },
                    input_startedat: {
                        required: 'Si us plau, introdueix un titol!'
                    },
                    input_price: {
                        required: 'Si us plau, introdueix un preu!',
                        number: 'Si us plau, introdueix solament números.'
                    },
                    input_endedat: {
                        required: 'Si us plau, introdueix una data!'
                    },
                    input_course: {
                        required: 'Si us plau, introdueix un curs!'
                    }
                },
            });
        }

        $('#table_id').DataTable({
            dom: 'Bflrtrip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
                }
            ],
            "language": {
                "sProcessing": "Processant...",
                "sLengthMenu": "Mostrar _MENU_ registres",
                "sZeroRecords": "No s'han trobat resultats",
                "sEmptyTable": "Cap dada disponible en aquesta taula",
                "sInfo": "Mostrant registres del _START_ al _END_ d'un total de _TOTAL_ registres",
                "sInfoEmpty": "Mostrant registres del 0 al 0 d'un total de 0 registres",
                "sInfoFiltered": "(Filtrat d'un total de _MAX_ registres)",
                "sInfoPostFix": "",
                "sSearch": "Cercar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargant...",
                "oPaginate": {
                    "sFirst": "Primer",
                    "sLast": "Últim",
                    "sNext": "Següent",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar per ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per ordenar la columna de manera descendent"
                }
            }
        });

        $('#table_id').on('click', '.delete', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            const successNotf = window.createNotification({
                theme: 'success',
                showDuration: 3000
            });
            const errorNotf = window.createNotification({
                theme: 'error',
                showDuration: 3000
            });

            if (borrarConfirm()) {
                myTable.row($(this).parents('tr'))
                    .remove()
                    .draw();

                $.ajax({
                    url: '/adminpayments/delete',
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        id: id,
                    },
                    dataType: "json",
                    success: function (data) {
                        successNotf({
                            message: 'Eliminat correctament.'
                        });
                    },
                    error: function () {
                        errorNotf({
                            message: "No s'ha pogut eliminar."
                        });
                    }
                });
            }
        });

        //DATA TABLE EDIT
        $('#table_id').on('click', '.edit', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            $.ajax({
                url: '/adminpayments/find',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    id: id,
                },
                dataType: "json",
                success: function (data) {
                    $("#input_id").val(data.id);
                    $("#input_title").val(data.title);
                    $("#input_order").val(data.order);
                    $("#input_description").val(data.description);
                    $("#input_price").val(data.price);
                    loadSubcategories(data.categories_id);
                    $("#input_startedat").val(data.started_at);
                    $("#input_endedat").val(data.ended_at);
                    $("#input_account").val(data.accounts_id);
                    $("#input_course").val(data.courses_id);
                    $("#input_category").val(data.categories_id);
                }
            });
        });

        $('#input_category').on('change', function () {
            var id = $(this).val();
            loadSubcategories(id);
        });

        function loadSubcategories(id){
            $.ajax({
                url: '/admincategories/getSubcategories',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    id: id,
                },
                dataType: "json",
                success: function (data) {
                    var $input_subcategory = $('#input_subcategory');
                    $input_subcategory.removeAttr('disabled');
                    $input_subcategory.html('');

                    for (var i = 0; i < data.length; i++) {
                        $("#input_subcategory").append(new Option(data[i].subcategory, data[i].id));
                    }
                }
            });
        }
    });

</script>
@endsection
