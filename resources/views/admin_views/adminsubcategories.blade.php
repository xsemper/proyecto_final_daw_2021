@extends('layouts.appadmin')

@section('content')
<!-- Comienzo del contenido de la pagina -->
<div class="container-fluid">
    <h1 class="h3 mb-4 text-gray-800">Subcategories</h1>
    <!-- Comienzo tabla -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Taula de Subcategories</h6>
            <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Afegir Subcategoria</button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="table_id" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Subcategoria</th>
                            <th>Categoria</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subcategories as $subcategory)
                        <tr>
                            <td>{{$subcategory->id}}</td>
                            <td>{{$subcategory->subcategory}}</td>
                            <td>{{isset($subcategory->category()->category) ? $subcategory->category()->category : '' }}
                            </td>
                            <td>
                                <a class="edit" data-id="{{$subcategory->id}}" data-toggle="modal"
                                    data-target="#exampleModal"><i class="fas fa-edit"></i></a>
                                <a class="delete" data-id="{{$subcategory->id}}"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Finalización tabla -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Afegir un pagament</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <form class="form-horizontal" id="formSubcategories1" name="formSubcategories" method="post"
                        action="{{url('/adminsubcategories/insertorupdate')}}" style="display: block;">
                        @csrf
                        <input type="hidden" name="input_id" id="input_id">

                        <div class="form-group mb-0">
                            <label for="input_subcategory">Subcategoria:</label>
                            <input class="form-control form-control-user" type="text" name="input_subcategory"
                                id="input_subcategory" placeholder="1r">
                        </div>

                        <div class="form-group mb-0">
                            <label for="input_categoria">A Categoria:</label>
                            <SELECT class="form-control form-control-user" name="input_categoria" id="input_categoria">
                                @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->category}}</option>
                                @endforeach
                            </SELECT>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" id="modal-close" name="modal-send"
                                data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="modal-send"
                                name="modal-send">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Finalización del contenido de la pagina -->

<script>
    $(document).ready(function () {
        //VALIDATION
        jQuery.validator.addMethod("noSpace", function (value, element) {
            return value == '' || value.trim().length != 0;
        }, "Si us plau, no fiquis separacions");
        jQuery.validator.addMethod("customEmail", function (value, element) {
            return this.optional(element) ||
                /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);
        }, "Please enter valid email address!");

        $.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || /^\w+$/i.test(value);
        }, "Letras, numeros y barra-bajas porfavor.");

        var $formSubcategories = $('#formSubcategories');

        if ($formSubcategories.length) {
            $formSubcategories.validate({
                rules: {
                    input_subcategory: {
                        required: true
                    }
                },
                messages: {
                    input_subcategory: {
                        required: 'Si us plau, introdueix una subcategoria!'
                    }
                },
            });
        }

        //DATA TABLE
        $('#table_id').DataTable({
            dom: 'Bflrtrip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
                }
            ],
            "language": {
                "sProcessing": "Processant...",
                "sLengthMenu": "Mostrar _MENU_ registres",
                "sZeroRecords": "No s'han trobat resultats",
                "sEmptyTable": "Cap dada disponible en aquesta taula",
                "sInfo": "Mostrant registres del _START_ al _END_ d'un total de _TOTAL_ registres",
                "sInfoEmpty": "Mostrant registres del 0 al 0 d'un total de 0 registres",
                "sInfoFiltered": "(Filtrat d'un total de _MAX_ registres)",
                "sInfoPostFix": "",
                "sSearch": "Cercar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargant...",
                "oPaginate": {
                    "sFirst": "Primer",
                    "sLast": "Últim",
                    "sNext": "Següent",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar per ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per ordenar la columna de manera descendent"
                }
            }
        });

        $('#table_id').on('click', '.delete', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            const successNotf = window.createNotification({
                theme: 'success',
                showDuration: 3000
            });
            const errorNotf = window.createNotification({
                theme: 'error',
                showDuration: 3000
            });

            if (borrarConfirm()) {
                myTable.row($(this).parents('tr'))
                    .remove()
                    .draw();

                $.ajax({
                    url: '/adminsubcategories/delete',
                    type: 'POST',
                    data: {
                        '_token': '{{ csrf_token() }}',
                        id: id,
                    },
                    dataType: "json",
                    success: function (data) {
                        successNotf({
                            message: 'Eliminat correctament.'
                        });
                    },
                    error: function () {
                        errorNotf({
                            message: "No s'ha pogut eliminar."
                        });
                    }
                });
            }
        });

        //DATA TABLE EDIT
        $('#table_id').on('click', '.edit', function () {
            var myTable = $('#table_id').DataTable();
            var id = $(this).attr('data-id');

            $.ajax({
                url: '/adminsubcategories/find',
                type: 'POST',
                data: {
                    '_token': '{{ csrf_token() }}',
                    id: id,
                },
                dataType: "json",
                success: function (data) {
                    $("#input_id").val(data.id);
                    $("#input_subcategory").val(data.subcategory);
                    $("#input_categoria").val(data.categories_id);
                }
            });
        });
    });

</script>
@endsection
