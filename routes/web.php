<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/* GUEST */
Route::get('/paymentsgallery/{id}', 'PaymentController@getPaymentsById')->name('guest_views.gallery');
Route::get('/paymentsgallery/show/{id}', 'PaymentController@show')->name('guest_views.costs');



/* ADMIN */

Route::group(['middleware' => 'auth'], function () {

    //categories blade
    Route::get('/admincategories/index', 'CategoryController@index')->name('admin_views.admincategories');
    Route::post('/admincategories/insertorupdate', 'CategoryController@insertorupdate')->name('admin_views.admincategories');
    Route::post('/admincategories/delete', 'CategoryController@delete')->name('admin_views.admincategories');
    Route::post('/admincategories/find', 'CategoryController@find')->name('admin_views.admincategories');
    Route::post('/admincategories/getSubcategories', 'CategoryController@getSubcategories')->name('admin_views.adminpayments');


    //subcategories blade
    Route::get('/adminsubcategories/index', 'SubcategoryController@index')->name('admin_views.adminsubcategories');
    Route::post('/adminsubcategories/insertorupdate', 'SubcategoryController@insertorupdate')->name('admin_views.adminsubcategories');
    Route::post('/adminsubcategories/delete', 'SubcategoryController@delete')->name('admin_views.adminsubcategories');
    Route::post('/adminsubcategories/find', 'SubcategoryController@find')->name('admin_views.adminsubcategories');

    // Route Payments
    Route::get('/adminpayments/index', 'PaymentController@index')->name('admin_views.adminpayments');
    Route::post('/adminpayments/insertorupdate', 'PaymentController@insertorupdate')->name('admin_views.adminpayments');
    Route::post('/adminpayments/delete', 'PaymentController@delete')->name('admin_views.adminpayments');
    Route::post('/adminpayments/find', 'PaymentController@find')->name('admin_views.adminpayments');


    // Route Courses
    Route::get('/admincourses/index', 'CourseController@index')->name('admin_views.admincourses');
    Route::post('/admincourses/insertorupdate', 'CourseController@insertorupdate')->name('admin_views.admincourses');
    Route::post('/admincourses/delete', 'CourseController@delete')->name('admin_views.admincourses');
    Route::post('/admincourses/find', 'CourseController@find')->name('admin_views.admincourses');

    // Route Accounts
    Route::get('/adminaccounts/index', 'AccountController@index')->name('admin_views.adminaccounts');
    Route::post('/adminaccounts/insertorupdate', 'AccountController@insertorupdate')->name('admin_views.adminaccounts');
    Route::post('/adminaccounts/delete', 'AccountController@delete')->name('admin_views.adminaccounts');
    Route::post('/adminaccounts/find', 'AccountController@find')->name('admin_views.adminaccounts');

    //costs blade routes
    Route::post('/costs/updatesm', 'PaymentController@updateSummernote')->name('guest_views.costs');

    //SuperAdmin roles (role === 2)
    Route::group(['middleware' => 'checkRole'], function() {
        // Route Users
        Route::get('/adminusers/index', 'UserController@index')->name('admin_views.adminusers');
        Route::post('/adminusers/update', 'UserController@update')->name('admin_views.adminusers');
        Route::post('/adminusers/delete', 'UserController@delete')->name('admin_views.adminusers');
        Route::post('/adminusers/find', 'UserController@find')->name('admin_views.adminusers');
        // Route::post('/adminusers/delete', 'UserController@delete')->name('admin_views.adminusers');
    });
});


//google
Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');
