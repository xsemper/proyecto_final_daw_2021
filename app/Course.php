<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $fillable = ['course', 'users_id', 'created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo('App\User', 'users_id')->first();
    }
}
