<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'accounts';

    protected $fillable = ['account', 'fuc', 'key', 'users_id', 'created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo('App\User', 'users_id')->first();
    }
}
