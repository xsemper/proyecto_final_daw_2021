<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategories';

    protected $fillable = ['subcategory', 'categories_id', 'users_id', 'created_at', 'updated_at'];

    public function user(){
        return $this->belongsTo('App\User', 'users_id')->first();
    }

    public function category(){
        return $this->belongsTo('App\Category', 'categories_id')->first();
    }
}
