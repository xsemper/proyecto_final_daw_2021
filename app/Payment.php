<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = ['title', 'order', 'description', 'price', 'started_at', 'ended_at', 'accounts_id', 'categories_id', 'subcategories_id', 'courses_id', 'users_id', 'created_at', 'updated_at'];

    //change id to name
    public function subcategory(){
        return $this->belongsTo('App\Subcategory', 'subcategories_id')->first();
    }

    public function category(){
        return $this->belongsTo('App\Category', 'categories_id')->first();
    }

    public function account(){
        return $this->belongsTo('App\Account', 'accounts_id')->first();
    }

    public function course(){
        return $this->belongsTo('App\Course', 'courses_id')->first();
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id')->first();
    }
}
