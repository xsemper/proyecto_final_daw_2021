<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use View; //View facade


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.appguest', \App\Http\ViewComposers\CategoryComposer::class);
        view()->composer('guest_views.gallery', \App\Http\ViewComposers\CourseComposer::class);
    }
}
