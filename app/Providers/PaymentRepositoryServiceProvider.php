<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\PaymentRepository;
use App\Repositories\RepositoryInterface;

class PaymentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(RepositoryInterface::class, PaymentRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
