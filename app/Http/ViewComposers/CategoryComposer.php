<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\CategoryRepository;
use App\Repositories\SubcategoryRepository;

class CategoryComposer
{
    protected $categories;

    /**
     * Create a new categories composer.
     *
     * @param  CategoryRepository $categories
     * @return void
     */
    public function __construct(CategoryRepository $categories, SubcategoryRepository $subcategories)
    {
        $this->categories = $categories;
        $this->subcategories = $subcategories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->categories->all()->sortBy('category'))->with('subcategories', $this->subcategories->all()->sortBy('subcategory'));
    }
}