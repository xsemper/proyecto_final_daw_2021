<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\CourseRepository;

class CourseComposer
{
    protected $courses;

    /**
     * Create a new courses composer.
     *
     * @param  CourseRepository $courses
     * @return void
     */
    public function __construct(CourseRepository $courses)
    {
        $this->courses = $courses;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('courses', $this->courses->all());
    }
}