<?php

namespace App\Http\Controllers;

use App\Repositories\CategoryRepository;
use App\Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;

class CategoryController extends Controller
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
	}

    public function show($id)
    {
        $category = $this->categoryRepository->find($id);
        return view('testview', compact('category'));
    }

    public function index()
    {
        $categories = $this->categoryRepository->all();
        return view('admin_views.admincategories', compact('categories'));
    }

    public function insertorupdate(Request $request){
        
        $validator = Validator::make($request->all(), [
            'input_categoria' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }

        $request->users_id = Auth::user()->id;

        if(Category::where('id', $request->input_id)->exists()){
            $this->categoryRepository->update($request, $request->input_id);
        }else{
            $this->categoryRepository->create($request);
        }

        return redirect('/admincategories/index');
    }

    public function delete(Request $request)
    {
        $this->categoryRepository->delete($request->id);
        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $category = $this->categoryRepository->find($request->id);
        return json_encode($category);
    }

    public function getSubcategories(Request $request)
    {
        $subcategories = $this->categoryRepository->getSubcategories($request->id);
        log::info("Controller: ". $subcategories);
        return json_encode($subcategories);
    }

}
