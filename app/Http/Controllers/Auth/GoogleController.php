<?php
  
namespace App\Http\Controllers\Auth;
  
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Socialite;
use Auth;
use Exception;
use App\User;
  
class GoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToGoogle()
    {   
        
        return Socialite::driver('google')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleGoogleCallback()
    {
        try {
            
            $user = Socialite::driver('google')->user();
     
            //check existence
            $finduser = User::where('google_id', $user->id)->first();
            $finduseremail = User::where('email', $user->email)->first();

            // only allow people with @inscamidemar.cat to login
            if(explode("@", $user->email)[1] !== 'inscamidemar.cat'){
                return redirect()->to('/login');
            }
     
            if($finduser)
            {
                if($finduser->status === 1)
                {
                    Auth::login($finduser);
                    return redirect('/home');
                }else{
                    return redirect('/login');
                }
                
            }else{
                
                if($finduseremail)
                {
                    $finduseremail->google_id = $user->id;
                    $finduseremail->save();

                    Auth::login($finduseremail);
                    return redirect('/home');
                }
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'google_id'=> $user->id,
                    'password' => encrypt('Superman_test'),
                    'role' => 1,
                    'status' => 0,
                ]);

                Auth::login($newUser);
     
                return redirect('/home');
            }
    
        } catch (Exception $e) {
            dd($e->getMessage());
        }
    }
}