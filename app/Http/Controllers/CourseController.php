<?php

namespace App\Http\Controllers;

use App\Repositories\CourseRepository;
use App\Course;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;

class CourseController extends Controller
{
    protected $courseRepository;

    public function __construct(CourseRepository $courseRepository)
	{
		$this->courseRepository = $courseRepository;
	}

    public function index()
    {
        $courses = $this->courseRepository->all();
        return view('admin_views.admincourses', compact('courses'));
    }

    public function insertorupdate(Request $request){
        
        $validator = Validator::make($request->all(), [
            'input_course' => ['required', 'regex:/[0-9]{4}-[0-9]{2}$/'],
        ]);
        
        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $request->users_id = Auth::user()->id;

        if(Course::where('id', $request->input_id)->exists()){
            $this->courseRepository->update($request, $request->input_id);
        }else{
            $this->courseRepository->create($request);
        }

        return redirect('/admincourses/index');
    }

    public function delete(Request $request)
    {
        $this->courseRepository->delete($request->id);
        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $course = $this->courseRepository->find($request->id);
        return json_encode($course);
    }
}
