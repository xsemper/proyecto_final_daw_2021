<?php

namespace App\Http\Controllers;

use App\Repositories\SubcategoryRepository;
use App\Repositories\CategoryRepository;
use App\Subcategory;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Auth;

class SubcategoryController extends Controller
{
    protected $subcategoryRepository;

    public function __construct(SubcategoryRepository $subcategoryRepository, CategoryRepository $categoryRepository)
	{
		$this->subcategoryRepository = $subcategoryRepository;
        $this->categoryRepository = $categoryRepository;
	}

    public function show($id)
    {
        $subcategory = $this->subcategoryRepository->find($id);
        return view('testview', compact('subcategory'));
    }

    public function index()
    {
        $subcategories = $this->subcategoryRepository->all();
        $categories = $this->categoryRepository->all();
        return view('admin_views.adminsubcategories', compact('subcategories', 'categories'));
    }

    public function insertorupdate(Request $request){

        $validator = Validator::make($request->all(), [
            'input_subcategory' => ['required'],
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }

        $request->users_id = Auth::user()->id;

        if(Subcategory::where('id', $request->input_id)->exists()){
            $this->subcategoryRepository->update($request, $request->input_id);
        }else{
            $this->subcategoryRepository->create($request);
        }

        return redirect('/adminsubcategories/index');
    }

    public function delete(Request $request)
    {
        $this->subcategoryRepository->delete($request->id);
        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $subcategory = $this->subcategoryRepository->find($request->id);
        return json_encode($subcategory);
    }
}
