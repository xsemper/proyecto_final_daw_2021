<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\User;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Auth;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}

    public function show($id)
    {
        $user = $this->userRepository->find($id);
        return view('testview', compact('user'));
    }

    public function index()
    {
        $users = $this->userRepository->all();
        return view('admin_views.adminusers', compact('users'));
    }

    public function update(Request $request){
        $request->users_id = Auth::user()->id;

        if(User::where('id', $request->input_id)->exists()){
            $this->userRepository->update($request, $request->input_id);
        }

        return redirect('/adminusers/index');
    }

    public function delete(Request $request)
    {
        $deleted = $this->userRepository->delete($request->id);

        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $user = $this->userRepository->find($request->id);
        return json_encode($user);
    }

}
