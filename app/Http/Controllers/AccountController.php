<?php

namespace App\Http\Controllers;

use App\Repositories\AccountRepository;
use App\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class AccountController extends Controller
{
    protected $accountRepository;

    public function __construct(AccountRepository $accountRepository)
	{
		$this->accountRepository = $accountRepository;
	}

	public function show($id)
    {
        $account = $this->accountRepository->find($id);
        return view('testview', compact('account'));
    }

    public function index()
    {
        $accounts = $this->accountRepository->all();
        return view('admin_views.adminaccounts', compact('accounts'));
    }

    public function insertorupdate(Request $request){

        $validator = Validator::make($request->all(), [
            'input_account' => ['required', 'string', 'max:255'],
            'input_fuc' => ['required', 'string', 'max:255'],
            'input_key' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }

        $request->users_id = Auth::user()->id;

        if(Account::where('id', $request->input_id)->exists()){
            $this->accountRepository->update($request, $request->input_id);
        }else{
            $this->accountRepository->create($request);
        }

        return redirect('/adminaccounts/index');
    }

    public function delete(Request $request)
    {
        $this->accountRepository->delete($request->id);
        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $account = $this->accountRepository->find($request->id);
        return json_encode($account);
    }
}
