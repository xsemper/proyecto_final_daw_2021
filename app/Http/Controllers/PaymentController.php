<?php

namespace App\Http\Controllers;

use App\Repositories\PaymentRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\SubcategoryRepository;
use App\Repositories\AccountRepository;
use App\Repositories\CourseRepository;

use App\Payment;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Auth;

class PaymentController extends Controller
{
    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository, CategoryRepository $categoryRepository, SubcategoryRepository $subcategoryRepository, AccountRepository $accountRepository, CourseRepository $courseRepository)
	{
		$this->paymentRepository = $paymentRepository;
        $this->categoryRepository = $categoryRepository;
        $this->subcategoryRepository = $subcategoryRepository;
        $this->accountRepository = $accountRepository;
        $this->courseRepository = $courseRepository;
	}

    public function index()
    {
        $payment = $this->paymentRepository->all();
        $categories = $this->categoryRepository->all();
        $subcategories = $this->subcategoryRepository->all();
        $accounts = $this->accountRepository->all();
        $courses = $this->courseRepository->all();
        return view('admin_views.adminpayments', compact('payment', 'categories', 'subcategories', 'accounts', 'courses'));
    }

    public function show($id)
    {
        $payment = $this->paymentRepository->find($id);
        return view('guest_views.costs', compact('payment'));
    }

    public function getPaymentsById($id)
    {
        $payments = Payment::where('subcategories_id', $id)->get();
        return view('guest_views.gallery', compact('payments'));
    }

    public function insertorupdate(Request $request){
        
        $validator = Validator::make($request->all(), [
            'input_title' => ['required', 'string', 'max:255'],
            'input_order' => ['required', 'string', 'max:255'],
            'input_price' => ['required', 'numeric', 'min:1','max:9999.99'],
            'input_startedat' =>['required', 'date'],
            'input_endedat' =>['required', 'date'],
        ]);

        if($validator->fails()){
            return back()->withErrors($validator)->withInput();
        }
        $request->users_id = Auth::user()->id;

        if(Payment::where('id', $request->input_id)->exists()){
            $this->paymentRepository->update($request, $request->input_id);
        }else{
            $this->paymentRepository->create($request);
        }

        return redirect('/adminpayments/index');
    }

    public function delete(Request $request)
    {
        $this->paymentRepository->delete($request->id);
        return json_encode('deleted');
    }

    public function find(Request $request)
    {
        $payment = $this->paymentRepository->find($request->id);
        return json_encode($payment);
    }

    public function updateSummernote(Request $request)
    {
        $payment = $this->paymentRepository->editDescription($request->id, $request->desc);
        return json_encode($payment->description);
        
    }

}
