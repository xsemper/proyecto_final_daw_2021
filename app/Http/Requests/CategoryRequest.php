<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return sssssssssssssssss
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "input_category" => 'required|numeric|digits:4'
        ];
    }

    public function messages() {
        return [
            'input_category.required' => 'La :attribute es obligatoria', 
            'input_category.digits' => 'La :attribute debe ser valida', 
            'input_category.numeric' => 'La :attribute debe ser valida'
        ];
    }

    public function attributes() {
        return [
            'input_category' => 'categoria'
        ];
    }
}
