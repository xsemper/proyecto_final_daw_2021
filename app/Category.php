<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['category', 'users_id', 'created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id')->first();
    }

    public function subcategories()
    {
        return $this->hasMany('App\Subcategory', 'categories_id');
    }
}
