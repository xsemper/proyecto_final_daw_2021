<?php

namespace App\Repositories;

use App\User;

use App\Repositories\RepositoryInterface;

use Illuminate\Support\Facades\Log;
use DB;

class UserRepository implements RepositoryInterface
{
	protected $user;

    public function __construct(User $user)
	{
		$this->user = $user;
	}

	public function all()
    {
        return $this->user->all();
    }

    public function create($data)
    {
        
    }
    

    public function update($data, $id)
    {
        return $this->user->where('id', $id)->update([
            'role' => $data->input_role,
            'status' => $data->input_status,
        ]);
    }

    public function delete($id)
    {
        /* Hard delete
        
        DB::table('payments')->where('users_id', '=', $id)->update(['users_id' => null]);
        DB::table('accounts')->where('users_id', '=', $id)->update(['users_id' => null]);
        DB::table('courses')->where('users_id', '=', $id)->update(['users_id' => null]);
        DB::table('categories')->where('users_id', '=', $id)->update(['users_id' => null]);
        DB::table('subcategories')->where('users_id', '=', $id)->update(['users_id' => null]);
        
        return $this->user->destroy($id); */
        
        
    
        //Soft delete
        return $this->user->where('id', $id)->update(['status' => 0]);
    }

	public function find($id)
    {
        if (null == $post = $this->user->find($id)) {
            throw new ModelNotFoundException("user not found");
        }

        return $post;
    }
}