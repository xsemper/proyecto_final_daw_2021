<?php

namespace App\Repositories;

use App\Course;

use App\Repositories\RepositoryInterface;

use Illuminate\Support\Facades\Log;
use DB;

class CourseRepository implements RepositoryInterface
{
	protected $course;

    public function __construct(Course $course)
	{
		$this->course = $course;
	}

	public function all()
    {
        return $this->course->all();
    }

    public function create($data)
    {
        $this->course->create([
            'course' => $data->input_course,
			'users_id' => $data->users_id,
		]);
    }

    public function update($data, $id)
    {
        return $this->course->where('id', $id)->update([
            'course' => $data->input_course,
            'users_id' => $data->users_id,
        ]);
    }

    public function delete($id)
    {
        DB::table('payments')->where('courses_id', '=', $id)->update(['courses_id' => null]);
        return $this->course->destroy($id);
    }

	public function find($id)
    {
        if (null == $post = $this->course->find($id)) {
            throw new ModelNotFoundException("course not found");
        }

        return $post;
    }

    //accedir eloquent model
    public function getModel() {
        return $this;
    }

}