<?php

namespace App\Repositories;

use App\Category;

use App\Repositories\RepositoryInterface;

use Illuminate\Support\Facades\Log;
use DB;

class CategoryRepository implements RepositoryInterface
{
	protected $category;

    public function __construct(Category $category)
	{
		$this->category = $category;
	}

	public function all()
    {
        return $this->category->all();
    }

    public function create($data)
    {
        $this->category->create([
            'category' => $data->input_categoria,
			'users_id' => $data->users_id,
		]);
    }

    public function update($data, $id)
    {
        return $this->category->where('id', $id)->update([
            'category' => $data->input_categoria,
            'users_id' => $data->users_id,
        ]);
    }

    public function delete($id)
    {
        DB::table('payments')->where('categories_id', '=', $id)->update(['categories_id' => null]);
        DB::table('subcategories')->where('categories_id', '=', $id)->update(['categories_id' => null]);
        return $this->category->destroy($id);
    }

	public function find($id)
    {
        if (null == $post = $this->category->find($id)) {
            throw new ModelNotFoundException("category not found");
        }

        return $post;
    }

    public function getSubcategories($id)
    {
        return $this->category->find($id)->subcategories()->get();
    }
}