<?php

namespace App\Repositories;

use App\Subcategory;

use App\Repositories\RepositoryInterface;

use Illuminate\Support\Facades\Log;
use DB;

class SubcategoryRepository implements RepositoryInterface
{
	protected $subcategory;

    public function __construct(Subcategory $subcategory)
	{
		$this->subcategory = $subcategory;
	}

	public function all()
    {
        return $this->subcategory->all();
    }

    public function create($data)
    {
        $this->subcategory->create([
            'subcategory' => $data->input_subcategory,
            'categories_id' => $data->input_categoria,
			'users_id' => $data->users_id,
		]);
    }

    public function update($data, $id)
    {
        return $this->subcategory->where('id', $id)->update([
            'subcategory' => $data->input_subcategory,
            'categories_id' => $data->input_categoria,
            'users_id' => $data->users_id,
        ]);
    }

    public function delete($id)
    {
        DB::table('payments')->where('subcategories_id', '=', $id)->update(['subcategories_id' => null]);
        return $this->subcategory->destroy($id);
    }

	public function find($id)
    {
        if (null == $post = $this->subcategory->find($id)) {
            throw new ModelNotFoundException("subcategory not found");
        }

        return $post;
    }
}