<?php

namespace App\Repositories;

use App\Payment;

use App\Repositories\RepositoryInterface;

use Illuminate\Support\Facades\Log;

class PaymentRepository implements RepositoryInterface
{
	protected $payment;

    public function __construct(Payment $payment)
	{
		$this->payment = $payment;
	}

	public function all()
    {
        return $this->payment->all();
    }

    public function create($data)
    {
        $this->payment->create([
            'title' => $data->input_title,
            'order' => $data->input_order,
            'price' => $data->input_price,
            'description' => $data->input_description,
			'started_at' => $data->input_startedat,
            'ended_at' => $data->input_endedat,
            'accounts_id' => $data->input_account,
            'categories_id' => $data->input_category,
            'subcategories_id' => $data->input_subcategory,
            'courses_id' => $data->input_course,
            'users_id' => $data->users_id,
		]);
    }

    public function update($data, $id)
    {
        return $this->payment->where('id', $id)->update([
            'order' => $data->input_order,
            'title' => $data->input_title,
            'price' => $data->input_price,
            'description' => $data->input_description,
            'started_at' => $data->input_startedat,
            'ended_at' => $data->input_endedat,
            'categories_id' => $data->input_category,
            'subcategories_id' => $data->input_subcategory,
            'accounts_id' => $data->input_account,
            'courses_id' => $data->input_course,
            'users_id' => $data->users_id,
        ]);
    }

    public function delete($id)
    {
        return $this->payment->destroy($id);
    }

	public function find($id)
    {
        if (null == $post = $this->payment->find($id)) {
            throw new ModelNotFoundException("payment not found");
        }

        return $post;
    }

    public function getBySubcategory($id)
    {
        return $this->payment->where('subcategories_id', $id)->first();
    }

    public function editDescription($id, $text)
    {
        $payment = $this->payment->find($id);
        $payment->description = $text;
        $payment->save();
        
        return $payment;
    }
}