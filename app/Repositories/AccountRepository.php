<?php

namespace App\Repositories;

use App\Account;

use Illuminate\Support\Facades\Log;
use DB;

class AccountRepository implements RepositoryInterface
{
	protected $account;

    public function __construct(Account $account)
	{
		$this->account = $account;
	}

	public function all()
    {
        return $this->account->all();
    }

    public function create($data)
    {
        $this->account->create([
            'account' => $data->input_account,
            'fuc' => $data->input_fuc,
            'key' => $data->input_key,
			'users_id' => $data->users_id,
		]);
    }

    public function update($data, $id)
    {
        return $this->account->where('id', $id)->update([
            'account' => $data->input_account,
            'fuc' => $data->input_fuc,
            'key' => $data->input_key,
            'users_id' => $data->users_id,
        ]);
    }

    public function delete($id)
    {
        DB::table('payments')->where('accounts_id', '=', $id)->update(['accounts_id' => null]);
        return $this->account->destroy($id);
    }

	public function find($id)
    {
        if (null == $post = $this->account->find($id)) {
            throw new ModelNotFoundException("Account not found");
        }

        return $post;
    }
}